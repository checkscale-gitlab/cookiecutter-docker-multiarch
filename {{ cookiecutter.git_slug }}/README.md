# {{ cookiecutter.docker_name }}/{{ cookiecutter.docker_slug }} [![Docker Image Size (latest semver)](https://img.shields.io/docker/image-size/{{ cookiecutter.docker_name }}/{{ cookiecutter.docker_slug }})](https://hub.docker.com/r/{{ cookiecutter.docker_name }}/{{ cookiecutter.docker_slug }}) [![Docker Pulls](https://img.shields.io/docker/pulls/{{ cookiecutter.docker_name }}/{{ cookiecutter.docker_slug }})](https://hub.docker.com/r/{{ cookiecutter.docker_name }}/{{ cookiecutter.docker_slug }}) [![Pipeline status](https://{{ cookiecutter.git_server }}/{{ cookiecutter.git_name }}/{{ cookiecutter.git_slug }}/badges/master/pipeline.svg)](https://{{ cookiecutter.git_server }}/{{ cookiecutter.git_name }}/{{ cookiecutter.git_slug }}/commits/master)

## Quick reference
- **Maintained by**: [{{ cookiecutter.author }}](https://{{ cookiecutter.git_server }}/{{ cookiecutter.git_name }})
- **Where to get help**: [Repository Issues](https://{{ cookiecutter.git_server }}/{{ cookiecutter.git_name }}/{{ cookiecutter.git_slug }}/-/issues)

## Description
{{ cookiecutter.description }}

## Usage
The simplest way to run the container is the following command:

```bash
docker run --detach {{ cookiecutter.docker_name }}/{{ cookiecutter.docker_slug }}
```

Or using `docker-compose.yml`:

```yaml
version: '3'
services:
  {{ cookiecutter.docker_slug }}:
    container_name: {{ cookiecutter.docker_slug }}
    image: {{ cookiecutter.docker_name }}/{{ cookiecutter.docker_slug }}
    restart: unless-stopped
```

## Contact
- [{{ cookiecutter.email }}](mailto:{{ cookiecutter.email }})
{%- if cookiecutter.git_server in ('gitlab.com', 'github.com') %}
- [incoming+{{ cookiecutter.git_name }}/{{ cookiecutter.git_slug }}@{{ cookiecutter.git_server }}](incoming+{{ cookiecutter.git_name }}/{{ cookiecutter.git_slug }}@{{ cookiecutter.git_server }})
{%- endif %}

## License
{{ cookiecutter.license }}

## Credits
This package was created with [Cookiecutter][cookiecutter] from [cookiecutter-docker-multiarch](https://gitlab.com/radek-sprta/cookiecutter-docker-multiarch).

[cookiecutter]: https://github.com/audreyr/cookiecutter
