# Cookiecutter: Docker Multiarch

Cookiecutter template for Docker multiarch images. Saves time by bootstrapping your repository and CI.

## Repository features

- Choose license
- Configure pre-commit hooks
- Setup Gitlab CI for building multiarch images

## Usage

To use the template, you have to install Cookiecutter. Simply use pip for the task:

`pip install cookiecutter`

And then run it to bootstrap your project:

`cookiecutter gl:radek-sprta/cookiecutter-docker-multiarch`

For more info, refer to the [documentation][documentation].

## Contributing

For information on how to contribute to the project, please check the [Contributor's Guide][contributing].

## Contact

[mail@radeksprta.eu](mailto:mail@radeksprta.eu)

[incoming+radek-sprta/cookiecutter-docker-multiarch@gitlab.com](incoming+radek-sprta/cookiecutter-docker-multiarch@gitlab.com)

## License

MIT License

## Credits

This package was created with [Cookiecutter][cookiecutter].

[contributing]: https://gitlab.com/radek-sprta/cookiecutter-docker-multiarch/blob/master/CONTRIBUTING.md
[cookiecutter]: https://github.com/audreyr/cookiecutter
[documentation]: https://radek-sprta.gitlab.io/cookiecutter-docker-multiarch
